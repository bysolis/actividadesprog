# -*- coding: utf-8 -*-

coleccion = [
    ["Bocas Del Toro", "Almirante", "Bocas del Toro", "Changuinola", "Chiriqui Grande"],
    ["Chiriqui", "Alanje", "Baru", "Boqueron", "Boquete", "Bugaba", "David", 
     "Dolega", "Gualaca", "Remedios", "Renacimiento", "San Félix", "San Lorenzo", "Tierras Altas", "Tolé"],
    ["Cocle", "Aguadulce", "Antón", "La Pintada", "Natá", "Olá", "Penonomé"],
    ["Colon", "Chagres", "Colón", "Donoso", "Portobelo", "Santa Isabel", "Omar Torrijos Herrera"],
    ["Darien", "Chepigana", "Pinogana", "Santa Fe", "Guna de Wargandí (comarca)"],
    ["Herrera", "Chitré", "Las Minas", "Los Pozos", "Ocú", "Parita", "Pesé", "Santa María"],
    ["Los Santos", "Guararé", "Las Tablas", "Los Santos", "	Macaracas", "Pedasí", "Pocrí", "Tonosí"],
    ["Panama", "Balboa", "Chepo", "Chimán", "Panamá", "San Miguelito", "Taboga"],
    ["Panama Oeste", "Arraiján", "Capira", "Chame", "La Chorrera", "San Carlos"],
    ["Veraguas", "Atalaya", "Calobre", "Cañazas", "La Mesa", "Las Palmas", "Mariato", "Montijo", "Río de Jesús",
     "San Francisco", "Santa Fe", "Santiago", "Soná"]
    ]

def normalize(s):
    replacements = (
        ("á", "a"),
        ("é", "e"),
        ("í", "i"),
        ("ó", "o"),
        ("ú", "u"),
    )
    for a, b in replacements:
        s = s.replace(a, b).replace(a.upper(), b.upper())
    return s

def buscar_provincia(provincia):
    global coleccion;
    existe = False
    for i in range(len(coleccion)):
        if coleccion[i][0].lower() == provincia:            
            for j in range(len(coleccion[i])):
                if j == 0:
                    print(f"Provincia [{coleccion[i][0]}]\nDistritos:")
                    existe = True
                else:
                    print(f"* {coleccion[i][j]}")
    return existe


opcion = 0

print("Nota: Ingresar tilde")
while opcion != 2:
    try:
        opcion = int(input("Opciones\n1) Consultar distritos por provincia\n2) Salir\nTu opción: "))
        if opcion == 1:
            provincia = input("Ingrese la provincia: ")
            if buscar_provincia(normalize(provincia).lower()) == False:
                print("No hay resultados para la provincia ingresada")
        elif opcion == 2:
            print("Saliendo...")
        else:
            print("Opción no válida")
    except ValueError as e:
        print(f"Ocurrió una excepción {e}")