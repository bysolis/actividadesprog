# -*- coding: utf-8 -*-

def es_numero(cadena):
    for i in range(len(cadena)):
        c = ord(cadena[i])
        if c < 48 or c > 57:
            return False
    return True

def es_letra(cadena):
    for i in range(len(cadena)):
        c = ord(cadena[i])
        if c < 65 or c > 71:
            return False
    return True

def evaluar(placa):
    global reglas
    for i in range(len(reglas)):
        cantidad_letras = reglas[i][2]
        letras = reglas[i][3]
        cantidad_digitos = reglas[i][1]
        if cantidad_letras > 0: #inicia con letras   
            cadena_inicio = placa[0:cantidad_letras] 
            print(f"cadena inicio {cadena_inicio} {letras}")
            cadena_fin = placa[cantidad_letras: len(placa)]
            if cadena_inicio == letras:
                if len(cadena_fin) == cantidad_digitos and es_numero(cadena_fin) == True:
                   return f"Es una placa tipo {reglas[i][0]}"
            elif cadena_inicio[0:1] == "A" and es_letra(cadena_inicio):
                if len(cadena_fin) == cantidad_digitos and es_numero(cadena_fin) == True:
                    return f"Es una placa tipo {reglas[i][0]}"
        
        elif reglas[i][5] == "si": #provincia si
            cadena_inicio = placa[1:2] 
            cadena_fin = placa[2: len(placa)]
            if es_numero(placa[0:1]) and placa[0:1] != "0" and cadena_inicio == letras:
                if len(cadena_fin) == cantidad_digitos and es_numero(cadena_fin) == True:
                   return f"Es una placa tipo {reglas[i][0]}" 
        elif reglas[i][5] == "no" and reglas[i][4] == "si": #provincia no - inicia con numero si
            if len(placa) == cantidad_digitos and es_numero(placa) == True:
                return f"Es una placa tipo {reglas[i][0]}" 
            
    return "No es una placa"
                
            

#Nombre, Digitos, Inicia con letra, Letras, Inicia con numero, numero es provincia
reglas = [
    ["Autoridad del Canal", 4,    2, "CP", "no", "no"],
    ["Cuerpo Diplomatico",  4,  2,  "CD", "no", "no"],
    ["Placa diplomatica",	3,	3,	"RCD", "no", "no"],
    ["Placa diplomatica",	3,	3,	"MCD", "no", "no"],
    ["Placa diplomatica",	2,	4,	"MADM", "no", "no"],
    ["Placa de administracion",	3,	3,	"ADM", "no", "no"],
    ["Placa diplomatica",	4,	2,	"PH", "no", "no"],
    ["Placa diplomatica",	4,	2,	"PE", "no", "no"],
    ["Placa diplomatica",	3,	3,	"MMI", "no", "no"],
    ["Placa diplomatica",	3,	3,	"RMI", "no", "no"],
    ["Periodista",	4,	2,	"PR", "no", "no"],
    ["Radioaficionados",	4,	2,	"HP", "no", "no"],
    ["Demostracion",	5,	1,	"D", "no", "no"],
    ["Fiscales y Jueces",	5,	1,	"E", "no", "no"],
    ["Metrobus",	4,	2,	"MB", "no", "no"],
    ["Motos",	4,	2,	"MA", "no", "no"],
    ["Particular de AA -> AG", 4,	2,	"AA", "no", "no"],
    ["Taxis",	4,	0,	"T",	"si",	"si"],
    ["Buses",	4,	0,	"B",	"si",	"si"],
    ["Bus escolar",	3,	0,	"BC",	"si",	"si"],
    ["Vehiculos del estado",	6,	0,	"-1",	"si",	"no"],
    ["Gobierno",	5,	1,	"G",	"no",	"no"],
]

opcion = 0
while opcion != 2:
    try:
        opcion = int(input("Opciones\n1) Verificar placa\n2) Salir\nTu opción: "))
        if opcion == 1:
            placa = input("Placa: ")
            print(evaluar(placa.upper()))
            
        elif opcion == 2:
            print("Saliendo...")
        else:
            print("Opción no válida")
    except ValueError as e:
        print(f"Ocurrió una excepción - Se esperaba un número {e}")


