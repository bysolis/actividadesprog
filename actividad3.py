# -*- coding: utf-8 -*-

class Tamagotchi:
    
    def __init__(self, nombre):
        self.nombre = nombre
        self.genero = "Sin genero"
        if nombre[len(nombre) - 1] == 'a':
            self.genero = "Femenino"
        else:
            self.genero = "Masculino"
        self.hambre = 10
        self.felicidad = 10
        self.salud = 0
    
    def alimentar(self):
        print("Alimentando")
        self.hambre = self.hambre + 2
        if self.hambre > 10:
            print("Sobre-alimentaste a Tamagotchi")
            self.salud = self.salud + 2
    
    def jugar(self):
        print("Jugando")
        self.felicidad = self.felicidad + 3
    
    def nada(self):
        print("No hace nada")
    
    def reducir_hambre_felicidad(self):
        self.felicidad = self.felicidad - 1
        self.hambre = self.hambre - 1
    
    def __str__(self):
        print(f"Tamagotchi [ nombre: {self.nombre}, género: {self.genero}, hambre: {self.hambre}, felicidad: {self.felicidad}, salud: {self.salud} ]")

def fin(tamagotchi):
    if tamagotchi.felicidad == 0 or tamagotchi.salud == 10 or tamagotchi.hambre == 0:
        return True
    return False

nombre = input("Nombre del Tamagotchi: ")

tamagotchi = Tamagotchi(nombre)

while fin(tamagotchi) == False:
    try:
        tamagotchi.__str__()
        opcion = int(input("Opciones:\n1) Alimentar\n2) Jugar\n3) Nada\nTu opción: "))
        print("")
        if opcion > 0 and opcion < 4:            
            if opcion == 1:
                tamagotchi.alimentar()
            elif opcion == 2:
                tamagotchi.jugar()
            elif opcion == 3:
                tamagotchi.nada()
                
            tamagotchi.reducir_hambre_felicidad()
        else:
            print("Opción no válida")
    except ValueError as e:
        print(f"Ocurrió una excepción - Se esperaba un número {e}")

print(f"Fin de Tamagotchi\n{tamagotchi.__str__()}")
                
    
    